#![allow(dead_code)]

extern crate wavefront_obj;
pub extern crate sp_mesh;
mod scene_graph;

pub mod io;
pub mod material;

use crate::scene_graph::{SceneGraph, MatId};
pub use scene_graph::{NodeId, MeshId};
use std::collections::HashMap;
use sp_mesh::mesh::Mesh;
use crate::sp_mesh::sp_math::matrix::Matrix4f;
use crate::material::Material;

type GroupId = usize;

pub struct Scene {
    scene_graph: SceneGraph,
    group_names: HashMap<NodeId, String>,
    mesh_names: HashMap<MeshId, String>,
}

impl Scene {
    pub fn new () -> Self {
        Self {
            scene_graph: SceneGraph::new(),
            group_names: HashMap::new(),
            mesh_names: HashMap::new(),
        }
    }

    pub fn add_group (&mut self, parent: NodeId, name: String) -> NodeId {
        let group = self.scene_graph.add_node(parent);
        self.group_names.insert(group, name);

        return group;
    }

    pub fn get_group_name (&self, group: NodeId) -> String {
        match self.group_names.get(&group) {
            None => {panic!("Invalid given group id: {}", group)},
            Some(name) => { name.clone() },
        }
    }

    pub fn add_mesh (&mut self, group: NodeId, name: String, mesh: Mesh) -> MeshId {
        let mesh_id = self.scene_graph.add_mesh(group, mesh);
        self.mesh_names.insert(mesh_id, name);

        return mesh_id;
    }

    pub fn set_mesh (&mut self, mesh_id: MeshId, mesh: Mesh) {
        self.scene_graph.set_mesh(mesh_id, mesh);
    }

    pub fn get_mesh_name (&self, mesh: MeshId) -> String {
        match self.mesh_names.get(&mesh) {
            None => {panic!("Invalid given mesh id: {}", mesh)},
            Some(name) => { name.clone() },
        }
    }

    pub fn get_meshes (&self) -> Vec<&Mesh> {
        self.scene_graph.get_meshes()
    }

    pub fn get_group_meshes (&self, group: NodeId) -> Vec<&Mesh> {
        self.scene_graph.get_node_meshes(group)
    }

    pub fn get_group_children (&self, group: NodeId) -> Vec<NodeId> {
        self.scene_graph.get_node_children(group)
    }

    pub fn get_group_parent (&self, group: NodeId) -> Option<NodeId> {
        self.scene_graph.get_node_parent(group)
    }
    pub fn get_mesh_transform (&self, mesh: MeshId) -> Matrix4f {
        let node = self.scene_graph.get_node(mesh);
        self.scene_graph.get_final_transformation(node)
    }

    pub fn add_material (&mut self, material: Material) -> MatId {
        self.scene_graph.add_material(material)
    }

    pub fn assign_material (&mut self, mesh_id: MeshId, material_id: MatId) {
        self.scene_graph.assign_material(mesh_id, material_id);
    }

    pub fn get_material (&self, mesh_id: MeshId) -> Option<&Material> {
        self.scene_graph.get_material(mesh_id)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
