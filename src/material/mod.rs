use sp_mesh::sp_math::vector::Vector3f;

#[derive(Clone)]
pub struct Material {
    pub name: String,
    pub specular_coefficient: f32,
    pub color_ambient: Vector3f,
    pub color_diffuse: Vector3f,
    pub color_specular: Vector3f,
    pub alpha: f32,
}

impl Material {
    pub fn new (name: String) -> Self {
        Self {
            name,
            specular_coefficient: 0.1,
            color_ambient: Vector3f::new(0.5,1.0,0.5),
            color_diffuse: Vector3f::new(0.5,1.0,0.5),
            color_specular: Vector3f::new(0.5,1.0,0.5),
            alpha: 1.0
        }
    }
}