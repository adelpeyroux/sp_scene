use wavefront_obj::obj::{parse, Primitive};

use std::ffi::OsString;
use std::fs;

use sp_mesh::mesh::Mesh;
use sp_mesh::mesh::vertex::Vertex;
use sp_mesh::mesh::face::Face;

use sp_mesh::sp_math::vector::{Vector3f, Vector2f};

use crate::Scene;
use crate::material::Material;
use std::collections::HashMap;

pub fn mat_from_obj (mat: &wavefront_obj::mtl::Material) -> Material {
    Material {
        name: mat.name.clone(),
        specular_coefficient: mat.specular_coefficient as f32,
        color_ambient: Vector3f::new(
            mat.color_ambient.r as f32,
            mat.color_ambient.g as f32,
            mat.color_ambient.b as f32
        ),
        color_diffuse: Vector3f::new(
            mat.color_diffuse.r as f32,
            mat.color_diffuse.g as f32,
            mat.color_diffuse.b as f32
        ),
        color_specular: Vector3f::new(
            mat.color_specular.r as f32,
            mat.color_specular.g as f32,
            mat.color_specular.b as f32
        ),
        alpha: 0.0
    }
}

pub fn from_file (obj_file: OsString) -> Result<Scene, String> {

    let file_content = fs::read_to_string(&obj_file)
        .expect("Something went wrong reading the file");

    let model = match parse(file_content.as_str()) {
        Ok(o) => {o},
        Err(err) => { return Err(err.message); },
    };

    let mut scene = Scene::new();

    let mut name_to_mat = HashMap::new();

    match model.material_library {
        None => {},
        Some(mat_lib_str) => {
            match fs::read_to_string(&mat_lib_str) {
                Ok(content) => {
                    match wavefront_obj::mtl::parse(content.as_str()) {
                        Ok(mat_lib) => {
                            for mat in mat_lib.materials.iter() {
                                name_to_mat.insert(
                                    mat.name.clone(),
                                    scene.add_material(mat_from_obj(mat))
                                );
                            }
                        },
                        Err(_) => {},
                    }
                },
                Err(_) => {},
            }
        },
    }

    for obj in model.objects.iter() {

        let vertices = &obj.vertices;
        let geometries = &obj.geometry;
        let normals = &obj.normals;
        let tex_coords = &obj.tex_vertices;

        if vertices.len() != 0 {
            let mut out_vertices = Vec::<Vertex>::with_capacity(vertices.len());
            let mut out_faces = Vec::<Face>::new();

            for v in vertices.iter() {
                out_vertices.push(Vertex::from_position(
                    Vector3f::new(v.x as f32, v.y as f32, v.z as f32)
                ));
            }

            let mut mat_name : &Option<String> = &None;

            for geo in geometries.iter() {
                mat_name = match mat_name {
                    None => { &geo.material_name },
                    Some(_) => { mat_name },
                };

                for shape in geo.shapes.iter() {
                    match shape.primitive {
                        Primitive::Point(_) => {},
                        Primitive::Line(_, _) => {},
                        Primitive::Triangle(v1, v2, v3) => {
                            out_faces.push(Face::new(v1.0, v2.0, v3.0));


                            if normals.len() != 0 {
                                match v1.2 {
                                    None => {},
                                    Some(id) => if id < normals.len() {
                                        out_vertices[v1.0].normal = Vector3f::new(
                                            normals[id].x as f32,
                                            normals[id].y as f32,
                                            normals[id].z as f32,
                                        ).normalized();
                                    },
                                };
                                match v2.2 {
                                    None => {},
                                    Some(id) => if id < normals.len() {
                                        out_vertices[v2.0].normal = Vector3f::new(
                                            normals[id].x as f32,
                                            normals[id].y as f32,
                                            normals[id].z as f32,
                                        ).normalized();
                                    },
                                };
                                match v3.2 {
                                    None => {},
                                    Some(id) => if id < normals.len() {
                                        out_vertices[v3.0].normal = Vector3f::new(
                                            normals[id].x as f32,
                                            normals[id].y as f32,
                                            normals[id].z as f32,
                                        ).normalized();
                                    },
                                };
                            }

                            if tex_coords.len() != 0 {
                                match v1.1 {
                                    None => {},
                                    Some(id) => if id < tex_coords.len() {
                                        out_vertices[v1.0].tex_coord = Vector2f::new(
                                            tex_coords[id].u as f32,
                                            tex_coords[id].v as f32,
                                        );
                                    },
                                };
                                match v2.1 {
                                    None => {},
                                    Some(id) => if id < tex_coords.len() {
                                        out_vertices[v2.0].tex_coord = Vector2f::new(
                                            tex_coords[id].u as f32,
                                            tex_coords[id].v as f32,
                                        );
                                    },
                                };
                                match v3.1 {
                                    None => {},
                                    Some(id) => if id < tex_coords.len() {
                                        out_vertices[v3.0].tex_coord = Vector2f::new(
                                            tex_coords[id].u as f32,
                                            tex_coords[id].v as f32,
                                        );
                                    },
                                };
                            }
                        },
                    }
                }
            }

            if out_faces.len() != 0 {
                let mut mesh = Mesh::build(out_vertices, out_faces);
                if normals.len() == 0 {
                    mesh = mesh.compute_normals();
                }

                let mesh_id = scene.add_mesh(0, obj.name.clone(), mesh);

                match mat_name {
                    None => {},
                    Some(name) => {
                        match name_to_mat.get(name) {
                            None => {},
                            Some(mat_id) => {
                                scene.assign_material(mesh_id, *mat_id);
                            },
                        }
                    },
                }
            }
        }
    }

    Ok(scene)
}