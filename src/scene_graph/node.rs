
use super::{NodeId, MeshId};

pub struct Node {
    pub parent: Option<NodeId>,
    pub meshes_id: Vec<MeshId>,
    pub children: Vec<NodeId>,
}
