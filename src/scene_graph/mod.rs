use crate::scene_graph::node::Node;
use sp_mesh::mesh::Mesh;
use std::collections::HashMap;
use sp_mesh::sp_math::matrix::Matrix4f;

mod node;

use crate::material::Material;

pub type NodeId = usize;
pub type MeshId = usize;
pub type MatId = usize;

pub struct SceneGraph {
    root: NodeId,
    nodes: Vec<Node>,
    meshes: Vec<Mesh>,
    materials: Vec<Material>,
    mesh_to_node: HashMap<MeshId, NodeId>,
    transforms: HashMap<NodeId, Matrix4f>,
    material_assignments: HashMap<NodeId, MatId>
}

impl SceneGraph {
    pub fn new () -> Self {
        let root_node = Node {
            parent: None,
            meshes_id: Vec::new(),
            children: Vec::new(),
        };

        Self {
            root : 0,
            nodes: vec!(root_node),
            meshes: Vec::new(),
            materials: Vec::new(),
            mesh_to_node: HashMap::new(),
            transforms: [(0, Matrix4f::identity())].iter().cloned().collect(),
            material_assignments: HashMap::new(),
        }
    }

    pub fn add_node (&mut self, parent: NodeId) -> NodeId {
        let result_id = self.nodes.len();

        if parent >= result_id {
            panic!("Invalid given parent id: {}", parent);
        }

        self.nodes.push(Node{
            parent: Some(parent),
            meshes_id: Vec::new(),
            children: Vec::new()
        });
        self.transforms.insert(result_id, Matrix4f::identity());

        self.nodes[parent].children.push(result_id);

        return result_id;
    }

    pub fn add_mesh (&mut self, node: NodeId, mesh: Mesh) -> MeshId {
        if node >= self.nodes.len() {
            panic!("Invalid given node id: {}", node);
        }

        let mesh_id = self.meshes.len();
        self.meshes.push(mesh);

        self.nodes[node].meshes_id.push(mesh_id);
        self.mesh_to_node.insert(mesh_id, node);

        return mesh_id;
    }

    pub fn set_mesh (&mut self, id: MeshId, mesh: Mesh) {
        if id >= self.meshes.len() {
            panic!("Invalid given mesh id: {}", id);
        }
        self.meshes[id] = mesh;
    }

    pub fn get_node (&self, mesh: MeshId) -> NodeId {
        if mesh >= self.meshes.len() {
            panic!("Invalid given mesh id: {}", mesh);
        }

        self.mesh_to_node.get(&mesh).unwrap().clone()
    }

    pub fn get_transformation (&self, node: NodeId) -> Matrix4f {
        if node >= self.nodes.len() {
            panic!("Invalid given node id: {}", node);
        }

        let result = match self.transforms.get(&node) {
            None => {
                panic!("No transform founds for given node id: {}", node)
            },
            Some(mat) => {
                mat.clone()
            },
        };

        // Linter may be triggered here but result is a Matrix4f and not a reference.
        return result;
    }

    pub fn get_final_transformation (&self, node: NodeId) -> Matrix4f {
        if node >= self.nodes.len() {
            panic!("Invalid given node id: {}", node);
        }
        let current_node = &self.nodes[node];

        let result = self.get_transformation(node) * match current_node.parent {
            None => {Matrix4f::identity()},
            Some(parent) => {self.get_final_transformation(parent)},
        };

        return result;
    }

    pub fn get_meshes (&self) -> Vec<&Mesh> {
        self.meshes.iter().map(|m| { m }).collect()
    }

    pub fn get_node_meshes (&self, node: NodeId) -> Vec<&Mesh> {
        if node >= self.nodes.len() {
            panic!("Invalid given node id: {}", node);
        }

        let current_node = &self.nodes[node];

        current_node
            .meshes_id
            .iter()
            .map(|id| { &self.meshes[id.clone()] })
            .collect::<Vec<&Mesh>>()
            .clone()
    }

    pub fn get_node_parent (&self, node: NodeId) -> Option<NodeId> {
        if node >= self.nodes.len() {
            panic!("Invalid given node id: {}", node);
        }

        self.nodes[node].parent.clone()
    }

    pub fn get_node_children (&self, node: NodeId) -> Vec<NodeId> {
        if node >= self.nodes.len() {
            panic!("Invalid given node id: {}", node);
        }

        self.nodes[node].children.clone()
    }

    pub fn add_material (&mut self, material: Material) -> MatId {
        let id = self.materials.len();

        self.materials.push(material);

        return id;
    }

    pub fn assign_material (&mut self, mesh_id: MeshId, material_id: MatId) {
        if mesh_id >= self.meshes.len() {
            panic!("Invalid given mesh id: {}", mesh_id);
        }

        if material_id >= self.materials.len() {
            panic!("Invalid given material id: {}", material_id);
        }

        self.material_assignments.insert(mesh_id, material_id);
    }

    pub fn get_material (&self, mesh_id: MeshId) -> Option<&Material> {
        if mesh_id >= self.meshes.len() {
            panic!("Invalid given mesh id: {}", mesh_id);
        }

        self.materials.get(mesh_id)
    }
}

